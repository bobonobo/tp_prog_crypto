#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <time.h>
#include "fonctions.h"

unsigned int EstPremier(long long int n){
	long long int r=racine(n);
	long long unsigned int rest;
	if((n==0)||(n==1)){
		//printf("%lld n'est pas premier d'après le crible\n",n);
		return 0;
	}
	if((n&1)==0){
		if(n==2){
			//printf("%lld est premier d'après le crible \n",n);
			return 1;		
		}
		else{
			//printf("%lld se divise par 2 d'après le crible\n",n);
			return 0;
		}
	}
	long long int i=3;
	while(i<=r){
		//DivPositif(&rest,n,i);
		rest=(n%i);
		if(rest==0){
			//printf("%lld se divise par %lld d'après le crible\n",n,i);
			return 0;		
		}		
		i+=2;
	}
	//printf("%lld est premier d'après le crible \n",n);
	return 1;
}

unsigned int TestPrimaliteFermat(long long unsigned int n,unsigned int K){
	if((n==0)||(n==1)){
		return 0;
	}
	if(n==2){
		return 1;
	}
	srand(time(NULL));
	long long int ra;
	long long unsigned int r;
	int i=1;
	int type=1;
	while((type==1)&&(i<K+1)){
		ra= rand();
		//DivPositif(&r,ra,(n-2));beaucoup plus lent à l'exécution que %(n-2);
		//r=r+2; 

		r=(ra%(n-2))+2;
		if(expmod2(r,n-1,n)!=1){
			type=0;
		}
		i++;
	}
	return type;

}

int main(int argc, char *argv[]){
	for(long long int i=1;i<12;i++){
		if(EstPremier(i)==1){
				printf("%lld est premier d'après le crible \n",i);
		}
		else{
				printf("%lld n'est pas premier d'après le crible\n",i);			
		}
		if(TestPrimaliteFermat(i,2)==1){		
			printf("%lld est probablement premier d'après Fermat \n",i);	
		}
		else{
			printf("%lld n'est pas premier d'après Fermat, \n",i);
		}
	}
	int p;
	int c;
	for(int b=10;b<21;b++){
		c=0;
		p=(1<<b);
		int ep;
		for(long long int i=1;i<p;i++){
			ep=1;
			int f = TestPrimaliteFermat(i,2);
			if(f==1){
				ep=EstPremier(i);
			}
			if(ep==0 && f==1){
				c++;
			} 
		}
		float est=(float)c/p;
		printf("estimation de pi: %f  \n",est);
	}
	return 0;
}


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <time.h>
#include "fonctions.h"

unsigned int TestMillerRabin(long long int a,long long int n){
	if((n==0)||(n==1)){
		return 0;
	}
	if(n==2){
		return 1;
	}
	if((n&1)==0){
		return 0;
	}
	long long int t=n-1;
	long long int s=0;
	while((t&1)==0){
		t=(t>>1);
		s++;
	}
	long long int b = expmod2(a,t,n);
	if(b==1){
		return 1;		
	}
	for(long long int i=0;i<s;i++){
		if(b==n-1){
			return 1;
		}
		if(b==1){
			return 0;		
		}
		b=expmod2(b,2,n);
	}
	return 0;
}

int main(int argc, char *argv[]){

	printf("Test de 2^561: %d\n",TestMillerRabin(2,561));

	printf("Test de 137^221: %d\n",TestMillerRabin(137,221));

	printf("Test de 174^221: %d\n",TestMillerRabin(174,221));

}
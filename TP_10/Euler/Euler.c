#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <time.h>
#include "fonctions2.h"
	
int elimDoublon(int c,long long int* rest,long long int* resf){
	int count=c;
	int i=0;
	while(i<c){
		int j=i+1;
		while(j<c){
			if(rest[j]!=rest[i]||(rest[i]==-1)){				
				j++;
			}
			else{
				rest[j]=-1;
				j++;
				count--;
			}		
		}		
		i++;		
	}
	int k=0;
	int j=0;
	while(k<count&&j<c){
		if(rest[j]!=-1){
			resf[k]=rest[j];
			k++;
			j++;
		}
		else{
			j++;
		}
	}
	return count;
}
int Legendre(long long unsigned int a, long long unsigned int p ){
	if(a==1){
		return 1;
	}
	if(a==0){
		return 1;
	}
	else if((a&1)==0){
		int r = Mult(p-1,p+1);
		if(((r>>3)&1)==1){
			return -Legendre((a>>1),p);
		}
		else{
			return Legendre((a>>1),p);
		}
	}
	else{
		int m = Mult(a-1,p-1);
		long long unsigned int r;
		DivPositif(&r, p, a);
		if(((m>>2)&1)==1){
			return -Legendre(r,a);
		}
		else{
			return Legendre(r,a);	
		}
	}
}

int main(int argc, char **argv){

	printf("phi(12):%lld\n",phi(12));
	printf("phi(10205):%lld\n",phi(10205));
	long long unsigned int n=(long long unsigned int)atoi(argv[1]);
	long long unsigned int rg=(n>>1);
	long long unsigned int sq;
	for(long long unsigned int i=1;i<=rg+1;i++){
		sq = sqrmod1(i,n);
		printf("%lld^2 mod %lld=%lld\n",i,n,sq);
	}
	long long int rest[15];
	for(long long unsigned int i=0;i<15;i++){
		rest[i]=(long long int)sqrmod1(i,29);
	}
	int count;
	long long int* resf=malloc(15*sizeof(long long int));
	count = elimDoublon(15,rest,resf);
	realloc( resf,sizeof(long long int)*count);
	//*

	for(int i=0;i<count;i++){
		printf("residus quadradiques méthode naïve %d :%lld\n",i,resf[i]);
	}
	free(resf);
	/*
	for(int i=0;i<15;i++){
		printf("residus quadradiques temporaires %d :%lld\n",i,rest[i]);
	}
	//*/
	long long int resuLeg[29];
	for(long long unsigned int i=0;i<29;i++){
		int l;
		if((l=Legendre(i,29))==1){
			resuLeg[i]=i;
		}
		else{
			resuLeg[i]=-1;	
		}
	}
	long long int* Leg=malloc(29*sizeof(long long int));
	int count2=elimDoublon(29,resuLeg,Leg);
	printf("count2 %d \n",count2);
	realloc( Leg,sizeof(long long int)*count2);		
	for(int i=0;i<count2;i++){
		printf("residus quadradiques méthode Legendre %d :%lld\n",i,Leg[i]);
	}
	//*


	

	return 0;


}
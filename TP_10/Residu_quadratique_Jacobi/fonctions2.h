#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <time.h>
int Taille(long long unsigned int x){
	if(x==0){
		return 0;
	}
	int i=31;
	while( (x&(1<<i))==0 &&(i>0)){
		i--;
	}
	return i+1;
}
long long unsigned int Mult(long long unsigned int a, long long unsigned int b){
	long long unsigned int p=0;
	for(int i=0;i<Taille(b);i++){
		if( ((b>>i)&1) ==1){
			p=p+(a<<i);	
		}		
	}
	return p;
}
long long unsigned int DivPositif(long long unsigned int *r, long long unsigned int a, long long unsigned int b){
	long long unsigned int pt;
	long long unsigned int gt;
	long long unsigned int q=0;
	if(b==1){
		*r=0;
		return a;
	}
	else if((a>b)){
		pt=b;
		gt=a;
	}
	else if((b>a)){
		*r=a;
		return 0;	
	}
	else if((b==a)){
		*r=0;
		return 1;
	}
	while(pt<=gt){
			gt=gt-pt;
			q++;
		}
	*r=gt;
	return q;		
}
long long unsigned int pgcd(long long unsigned int x, long long unsigned int y){
	long long unsigned int a=x;
	long long unsigned int b=y;
	long long unsigned int d=0;
	long long unsigned int abs;
	long long unsigned int min;
	while(a!=b){
		if( (a&1)==0 && (b&1)==0 ){
			a=(a>>1);
			b=(b>>1);
			d++;
		}
		else if((a&1)==0 && (b&1)==1 ){
			a=(a>>1);
		}
		else if((a&1)==1 && (b&1)==0 ){
			b=(b>>1);
		}
		else{
			abs = (a-b)*(a>b)+(b-a)*(b>=a);
			min = a*(b>a)+b*(b<=a);
			a=abs;
			b=min;	
		}

	}

	return (1<<d)*a;

}
long long unsigned int Abs(long long int x){
 long long int a = (x>=0)*x-(x<0)*x;
 return (long long unsigned int) a; 	
}

long long unsigned int mulmod(long long unsigned int a, long long unsigned int n, long long unsigned int m ){
	long long unsigned int r1;
	long long unsigned int r2;
	long long unsigned int r3;
	DivPositif(&r1,a,m);
	DivPositif(&r2,n,m);
	r3=Mult(r1,r2);
	DivPositif(&r3,r3,m);
	return r3;

}

long long unsigned int sqrmod1(long long unsigned int a, long long unsigned int m){
	long long unsigned int r1;
	long long unsigned int r2;
	DivPositif(&r1,a,m);
	r2=Mult(r1,r1);
	DivPositif(&r2,r2,m);
	return r2;	
}

long long unsigned int sqrmod2(long long unsigned int a, long long unsigned int m){
	return mulmod(a,a,m);		
}
long long unsigned int expmod1(long long unsigned int a,long long unsigned int n,long long unsigned int m){
	if((n&1)==0 && n>0){
		return sqrmod1(expmod1(a,(n>>1),m),m);
	}
	else if((n&1)==1 && n>0){
		return mulmod(a,expmod1(a,n-1,m),m);
	}
	if (n==0){
		return 1;
	}
	else return -1;
}
long long unsigned int expmod2(long long unsigned int a,long long unsigned int n,long long unsigned int m){
	int t= Taille(n);
	printf("n: %lld, taille de n:%d\n",n,t);
	long long unsigned int p=1;
	long long unsigned int asq =a;//sqrmod1(a,m);
	if (n==0){
		return 1;
	}
	if((n&1)==1){	
		p=a;
	} 
	for (int i=1;i<t;i++){
		asq=sqrmod1(asq,m);
		if(((n>>i)&1)==1){
			p=mulmod(p,asq,m);
		}		
	}
	return p;
}
long long unsigned int expmod3(long long unsigned int a,long long unsigned int n,long long unsigned int m){
	int t= Taille(n);
	long long unsigned int p=1;
	long long unsigned int asq =a;//sqrmod1(a,m);
	if (n==0){
		return 1;
	}
	if((n&1)==1){	
		p=a;
	} 
	int i=1;
	while (i<t){
		asq=sqrmod1(asq,m);
		if(((n>>i)&1)==1){
			p=mulmod(p,asq,m);
		}		
		i++;
	}
	return p;
}
long long unsigned int BezoutBinaire(long long int*pu,long long int*pv,long long unsigned int a,long long unsigned int b){
	long long unsigned int r0;
	long long unsigned int r1;
	if(a<b){
		r0 = a;
		r1 = b;
	}
	else{
		r0 = b;
		r1 = a;	
	}
	long long int u0 = 1;
	long long int u1 = 0;
	long long int v0 = 0;
	long long int v1 = 1;
	long long unsigned int q;
	long long unsigned int rt0;
	long long unsigned int rt1;
	long long int ut0;
	long long int ut1;
	long long int vt0;
	long long int vt1;
	long long unsigned int r;
	while((r1!=0) && (r0!=0)){
		q=DivPositif(&r,r0,r1);
		rt0=r0;
		rt1=r1;
		r0=rt1;
		r1=rt0-Mult(q,rt1);
		ut0=u0;
		ut1=u1;
		u0=ut1;
		u1=ut0-Mult(q,ut1);
		vt0=v0;
		vt1=v1;
		v0=vt1;
		v1=vt0-Mult(q,vt1);
	}
	if(r0==0){
		*pu=ut0;
		*pv=vt0;
		return rt0; 
	}
	else{
		*pu =u0;
		*pv =v0;
		return r0;
	}
	
	

}
long long unsigned int racine(long long unsigned int n){
	long long unsigned int x=n;
	long long unsigned int y=1;
	long long unsigned int r;
	while(x>y){
		x=(x+y)>>1;
		y=DivPositif(&r,n,x);
	}
	return x;
}

long long unsigned int phi(long long unsigned int n){
	long long unsigned int nt=n;
	long long unsigned int p0;
	long long unsigned int p1;
	long long unsigned int phit=1;
	long long int i = 2;
	while(i<=n){
		long long unsigned int r=0;
		long long unsigned int q;
		p0=0;
		p1=1;
		while(r==0){				
			q= DivPositif(&r,nt,i);
			if(r==0){
				//pt=p0;
				p0=p1;
				p1=Mult(p1,i);
				nt=q;
			}
		}
		
		if(p1!=p0){
			phit=Mult(phit,p1-p0);
		}
		i++;
	}
	return phit;
}


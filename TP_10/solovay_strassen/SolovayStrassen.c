#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <time.h>
#include "fonctions2.h"
//premier exercice dans fonctions2.h

void Test(long long int n, long long int a){
if(n<0){
		n=-n;
	}
	if(n==1||n==0){
		printf("%lld n'est ni composé ni premier\n",n);
		return;
	}
	if(n==2||n==3){
		printf("%lld est premier\n",n);
		return;
	}
	if((n&1)==0){
		printf("%lld est composé\n",n);
		return;
	}
	long long int p=((n-1)>>1);
	int j;	
	long long int e;
	e=expmod2(a,p,n);
	j=jacobi(a,n);		
	if( (j==e)||(j==e-n) ){
		printf("%lld est peut-être premier\n",n);			
	}	
	else{
		printf("%lld est composé\n",n);
		return;		
	}

}


int main(int argc, char ** argv){
	long long int ra;
	for(long long int i=0;i<102;i++){
		srand(time(NULL));	
		ra= (rand()%(98))+2;
		Test(i,ra);	
	}
	return 0;
}
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <time.h>
#include "Gauss.h"

GaussType phi={1,1,2};
GaussType phiChap={1,-1,2};

typedef struct{
	int a;
	int b;
}Frac;
void RedFrac(Frac *x){
	int p =pgcd((long int)x->a,(long int)x->b);
	if(p!=1){
		x->a=(x->a)/p;
		x->b=(x->b)/p;
	}
	if(x->b<0){
		x->a=-x->a;
		x->b=-x->b;
	}

}


unsigned long int Fibonacci1(unsigned int n){
	GaussType phiN; 
	GaussPuiss(&phiN,phi,n);
	GaussType phiNC;
	GaussPuiss(&phiNC,phiChap,n);
	unsigned long int F;
	GaussType Ft;
	
	GaussSoustrait(&Ft,phiN,phiNC);
	Reduire(&Ft);
	F=Ft.b;
	return F;
}

int Fibonacci2(unsigned int n){
	double vn=0.5;
	double t;
	double vn1=0.5;
	double t1;
	double vn2 =1;
	double t2;
	int ind = 1;
	if(n==0){
		return 0;
	}
	if(n==1){
		return 1;
	}
	if(n==2){
		return 1;
	}
	while(2*ind<=n){
		t=4*vn*vn1-2*vn*vn;
		t1=2*vn1*vn1+2*vn*vn;
		vn=t;
		vn1=t1;
		vn2=vn1+vn;
		ind=(ind<<1);
	}
	while(ind<n){
		t1=vn1;
		t2=vn2;
		vn=t1;
		vn1=t2;
		vn2=vn1+vn;
		ind++;
	}	
	int F= (int)2.*vn;
	return F;
}


int main(int argc, char*argv[]){
	int n=atoi(argv[1]);
	unsigned long int F = Fibonacci1(n);
	printf("F%d = %ld\n",n,F);
	printf("21 premiers termes de Fibonacci1\n");
	clock_t start,end;
	double t,t2;
	start=clock();
	for(int i=0;i<21;i++){
		printf("F%d = %ld\n",i,Fibonacci1(i));	
	}
	end=clock();
	t=((double)(end-start))/CLOCKS_PER_SEC;
	printf("t1:%f\n",t);
	printf("21 premiers termes de Fibonacci2\n");
	start=clock();
	for(int i=0;i<21;i++){
		printf("F%d = %d\n",i,Fibonacci2(i));	
	}
	end=clock();
	t2=((double)(end-start))/CLOCKS_PER_SEC;
	printf("t2:%f\n",t2);
	return 0;
}


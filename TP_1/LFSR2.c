#include <stdio.h>
#define L 9

unsigned int Etat;
unsigned int Polynome = 0xc9;
void Initialise(unsigned int e){
	Etat=e;
}
void AfficheEtat(){
	for(int i =L-1;i>-1;i--){
		printf("%2d",(Etat>>i)&1);
	}
	printf("\n");

}

unsigned int Rebouclage(){
	unsigned int x;
	x=Etat & Polynome;//chaque bit ne vaut 1 que si les deux valent 1 equivalent à E(x)P
	x^=(x>>8);//calcul le dernier bit par une espèce de dichotomie
	x^=(x>>4);
	x^=(x>>2);
	return (x^(x>>1))&1;//retourne (x xor (x/2)) si ?? 1?
}
void Avance(){
	//unsigned int temp=(Rebouclage()<<8);
	Etat=(Etat>>1)+(Rebouclage()<<8);
}

int main(int argc, char* argv[]){
	unsigned int EtatInitial=0x001;
	Initialise(EtatInitial);
	printf("Série de 20 états: \n ");
	for(int i=0;i<20;i++){
		AfficheEtat();
		Avance();
	}

	Initialise(EtatInitial);
	Avance();
    int n=1;
	while(Etat!=EtatInitial){
		n++;
		Avance();
	}
	printf("Période trouvée avec LFSR2: %d \n ", n);

	return 0;
}
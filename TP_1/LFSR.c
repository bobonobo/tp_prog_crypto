#include <stdio.h>
#define L 9

int Etat[L];
int Polynome[L]={1,0,0,1,0,0,1,1,0};
//initialisation
void Initialise(int* e){

for(int i=0;i<L;i++){
		Etat[i]=e[i];
	}

}
void AfficheEtat(){
printf("{");
	for(int i=L-1;i>0;i--){
		printf("%2d", Etat[i]);	
	}
printf("%2d ", Etat[0]);
printf("}");
printf("\n");
}
void Avance(){
	int temp=0;
	for(int i=0;i<L;i++){
		if(Polynome[i]==1){
			temp^= Etat[i];
		}
		
	}		
	for(int i=0;i<L-1;i++){
		Etat[i]=Etat[i+1];
	}
	Etat[L-1]=temp;
}
int SontEgaux(int* a,int* b){
//on pourrait déjà contrôler qu'ils ont la même taille
int i=0;
while ((a[i] == b[i]) && (i<L-1) ){
	i++;
	if ((i==L-1)&& (a[i] == b[i]))
	{
		return 1;
	}
}
return 0;
}

int main(int argc, char *argv[]){
//1-c	
int EtatInitial[L]={1,0,0,0,0,0,0,0,0};
Initialise(EtatInitial);
for(int i=0;i<20;i++){
	AfficheEtat();
	Avance();
}
//1-d
Initialise(EtatInitial);
Avance();
int n=1;
while(SontEgaux(Etat,EtatInitial)==0){
	n++;
	Avance();
	}
printf("Période %d \n",n);	

}
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>


/*Exo 0
1)	a)dist(f,g)= TF(f+g)(0)
*/
 	//b)
void Fourier_It(int* tab, int n){
	unsigned int nn=1<<n;
	unsigned int rr=nn>>1;
	int i;
	int t;
	do{
		i=0;
		do{
			t=tab[i];
			tab[i]=tab[i+rr]+tab[i];
			tab[i+rr]=t-tab[i+rr];
			i++;
			if((i&(rr-1))==0){
				i+=rr;
			}
		}
		while(i<nn);
		rr>>=1;
	}
	while(rr!=0);
}

int dist1( int *f, int *g, int n){
	unsigned int nn = 1<<n;
	int h[nn];
	for(int i = 0; i<nn;i++){
		h[i]=f[i]^g[i];
	}
	Fourier_It(h,n);

	return h[0];

 }

void Walsh_It(int *f, int n){
	unsigned int nn = 1<<n;
	for(int i = 0; i<nn;i++){
		f[i]=f[i]+f[i]-1;
	}
	Fourier_It(f,n);
}
/*
2)	a) dist(f,g)= 2^(n-1)-1/2*X(f+g)(0)
*/
 	//b)


int dist2( int *f, int *g, int n){
	unsigned int nn = 1<<n;
	int h[nn];
	for(int i = 0; i<nn;i++){
		h[i]=f[i]^g[i];
	}
	Walsh_It(h,n);
	nn>>=1;
	return nn-(h[0]>>1);
 }


/*
dH(f,phi_{a,b})=2^(n-1)-1/2 \Khi_{f+phi_{a,b}}(0)
\Khi_{f+phi_{a,b}}(0) = \Sum_x (-1)^{f+phi_{a,b}}(-1)^{<0,x>}
					  = \Sum_x (-1)^{f+phi_{a,b}}
					  = \Sum_x (-1)^f (-1)^phi_{a,b}
					  = \Sum_x (-1)^f(-1)^{<a,x>\xor b}
					  = (-1) ^b\Sum_x (-1)^f(-1)^{<a,x>}
					  = (-1) ^b\Khi_{f}(a)

min( 2^(n-1)-1/2 (-1)^b\Khi_{f}(a) )= 2^(n-1)-1/2max((-1)^b\Khi_{f}(a))
									= 2^(n-1)-1/2max(|\Khi_{f}(a)|)


*/

unsigned int NL(int *f, int n){
	unsigned int nn = 1<<n;
	int h=0;
	Walsh_It(f,n);
	for(int i = 0; i<nn;i++){
		if( h < ( f[i]*( (f[i]>=0)-(f[i]<0) ) ) ){
			h = ( f[i]*( (f[i]>=0)-(f[i]<0) ) );
		}
	}
	nn>>=1;
	return nn-(h>>1)*(h!=0);
}
unsigned int Pi(unsigned int x){
	int pi[8]={0x0b,0x07,0x1e,0x1b,0x1d,0x0f,0xff,0x17};
	return pi[x];
}
/*
unsigned int Y(unsigned int y){
	int v[32];
	for(int i =0;i<32;i++){
		v[i]=i;
	}
	return v[y];
}
*/
unsigned int ProdScalPi(unsigned int x,unsigned int y){
	unsigned long int t= Pi(x)&y;
	unsigned int p=0;
	while(t!=0){
		if((t&1)==1){
			p^=1;
				
		}
		t=(t>>1);
	}
	return p;
}
unsigned int ProdScal(unsigned int x,unsigned int y){
	unsigned long int t= x&y;
	unsigned int p=0;
	while(t!=0){
		if((t&1)==1){
			p^=1;
				
		}
		t=(t>>1);
	}
	return p;
}

int main(int argc, char **argv){
	int f[256];
	for(int i=0;i<8;i++){
		for(int j=0;j<32;j++){
			f[32*i+j]=ProdScalPi(i,j);
		}
	}
	int nl=NL(f,8);

	printf("%d",nl);
	printf("\n");

 	int g[512]={1,0,0,1,0,1,1,1,0,1,1,1,1,1,1,1,0,0,1,1,1,1,1,1,1,1,1,1,1,0,1,0,0,0,0,0,1,1,1,0,1,1,1,1,1,0,1,0,1,0,1,0,1,1,1,0,1,1,0,0,1,0,0,1,
				0,1,0,1,0,1,0,1,1,1,1,1,1,0,0,0,1,1,1,1,1,0,1,0,1,1,0,0,1,1,0,1,1,1,0,0,1,1,0,0,1,0,1,0,1,0,0,1,1,0,1,0,0,0,0,0,1,0,0,0,0,0,1,1,
				0,1,1,1,0,1,1,0,0,1,1,0,0,1,1,0,1,1,1,0,1,0,1,1,1,1,0,0,0,0,0,0,1,1,1,1,1,0,1,0,1,0,0,0,1,0,0,0,1,1,1,0,0,0,0,0,1,0,1,1,0,0,1,1,
				1,1,1,1,0,1,0,0,1,1,1,0,0,0,0,0,1,0,0,0,1,0,0,1,1,0,0,0,0,0,1,1,1,1,0,0,1,0,0,0,0,1,0,0,0,1,0,1,1,0,0,1,0,0,0,1,0,1,0,1,1,1,1,0,
				0,1,1,1,1,1,1,1,0,1,1,1,1,1,0,0,0,0,1,0,1,1,0,0,0,0,1,0,1,0,0,1,1,1,1,1,1,1,0,0,1,1,0,0,1,0,1,1,1,0,1,0,0,0,0,1,0,0,0,0,0,0,0,1,
				1,1,1,0,1,0,1,0,1,0,0,1,1,0,0,0,1,1,0,0,0,0,0,0,1,0,0,0,0,1,0,1,1,1,1,0,1,0,0,0,0,0,0,1,0,0,0,1,1,0,0,0,1,0,1,1,0,1,0,1,1,1,1,0,
				1,1,1,1,1,1,1,0,0,0,1,0,0,0,0,1,1,1,1,0,1,0,0,1,0,0,0,1,0,0,0,1,1,0,0,0,0,1,0,0,1,0,0,0,0,0,1,1,1,0,0,0,0,1,0,1,0,0,0,1,1,1,1,0,
				1,1,1,0,0,0,0,1,1,0,0,1,0,1,0,1,0,0,1,0,0,0,0,1,0,0,1,1,0,1,1,0,1,0,0,1,0,1,1,1,0,0,0,1,0,1,1,0,0,1,1,1,0,1,1,0,1,1,1,0,1,0,0,1};

	int nl2=NL(g,9);

	printf("%d",nl2);
	printf("\n");	
	int p[256];
	for(int i=0;i<16;i++){
		for(int j=0;j<16;j++){
			p[16*i+j]=ProdScal(i,j);
		}
	}
	int nl3=NL(p,8);

	printf("%d",nl3);
	printf("\n");	
	
	return 0;
}


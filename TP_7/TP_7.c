#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>

typedef struct{
	long int a;
	long int b;
	long int d;
}GaussType;

void GaussAffiche(GaussType x,int cr){
	if(x.a==0 && x.b==0){
		printf("%d %s",0,cr?"\n":" ");
	}
	else if(x.a==x.d && x.b==0){
		printf("%d %s",1,cr?"\n":" ");
	}
	else{
		printf("(%ld %s %ld √5) / %ld%s",
		x.a,
		x.b<0?"-":"+", // on affiche a - b V5 si b est n\’egatif
		x.b<0?-x.b:x.b,
		x.d,
		cr?"\n":" ");
	}
}
// Algorithme d’Euclide

long int pgcd(long int a, long int b){
// valeurs absolues de x et y
	if (a<0) a=-a;
	if (b<0) b=-b;
	if (b==0) return a; // cette fonction rend donc 0 si a et b sont
// tous les deux nuls. Il suffit de le savoir.
while (a){
	b%=a;
	if (b==0) return a;
	a%=b;
	}
	return b;
}
long unsigned int min(long unsigned int a, long unsigned int b){
	long unsigned int q;
	a<b?q=a:b;
	return q;
}
void Reduire(GaussType *x){
	long int m =pgcd(x->a,x->b);
	long int g= pgcd(x->d,m);
	if(g!=1){
		x->a=(x->a)/g;
		x->b=(x->b)/g;
		x->d=(x->d)/g;
	}
	if(x->d<0){
		x->a=-x->a;
		x->b=-x->b;
		x->d=-x->d;
	}

}
void GaussAdd(GaussType *s,GaussType x,GaussType y){
	s->a=(x.a*(y.d)+y.a*(x.d));
	s->b=(x.b*(y.d)+y.b*(x.d));
	s->d=(x.d)*(y.d);
	Reduire(s);
}
void GaussMul(GaussType *p,GaussType x,GaussType y){
	GaussType t;
	t.a=(x.a*y.a +5*x.b*y.b );
	t.b=(x.a*y.b +x.b*y.a );
	t.d=(x.d)*(y.d);
	p->a=t.a;
	p->b=t.b;
	p->d=t.d;
	Reduire(p);
}

void GaussOpp(GaussType *o, GaussType x){
	o->a=-x.a;
	o->b=-x.b;
    o->d=x.d;
    Reduire(o);
}
void GaussInv(GaussType *i, GaussType x){
	if(x.a!=0||x.b!=0){
		i->a=x.a*x.d;
		i->b=-x.b*x.d;
		i->d=x.a*x.a-x.b*x.b*5;
    	Reduire(i);
	}
	else{
		printf("division par 0");
		return;
	}
}
void GaussSoustrait(GaussType *s, GaussType x, GaussType y){
	GaussType t;
	GaussOpp(&t,y);
	GaussAdd(s, x, t);
}
void GaussDivise(GaussType *d, GaussType x, GaussType y){
	GaussType t;
	GaussInv(&t,y);
	GaussMul(d, x, t);
}
//*
void GaussPuiss(GaussType *r, GaussType x, unsigned int n){	
	GaussType p;
	GaussType y;
	y.a=x.a;
	y.b=x.b;
	y.d=x.d;
	p.a=1;
	p.b=0;
	p.d=1;
	unsigned int ex=n;	
	while(ex!=0){
		//if( ((ex>>1)&1) ==1){
		if( (ex&1) ==1){
			GaussMul(&p,p,y);
		}
		GaussMul(&y,y,y);
		ex=(ex>>1);
		}

	Reduire(&p);
    r->a=p.a;
    r->b=p.b;
    r->d=p.d;
    return;        
}

int main(int argc, char **argv){
	
	GaussType m={9,-72,-45};
	GaussAffiche(m,1);
	Reduire(&m);
	GaussAffiche(m,1);
	GaussType x={-1,4,45};
	GaussType y={-6,7,100};
	GaussType s,p;
	GaussAdd(&s,x,y);
	GaussMul(&p,x,y);
	printf("Somme = ");
	GaussAffiche(s,1);
	printf("Produit = ");
	GaussAffiche(p,1);
	GaussType o,i,r;
	GaussOpp(&o,x);
	printf("Oppose = ");
	GaussAffiche(o,1);
	printf("Verification : ");
	GaussAffiche(x,0);
	printf(" + ");
	GaussAffiche(o,0);
	GaussAdd(&r,x,o);
	printf(" = ");
	GaussAffiche(r,1);
	GaussInv(&i,x);
	printf("Inverse = ");
	GaussAffiche(i,1);
	printf("Verification : ");
	GaussAffiche(x,0);
	printf(" x ");
	GaussAffiche(i,0);
	GaussMul(&r,x,i);
	printf(" = ");
	GaussAffiche(r,1);
	GaussType so,d,pu;
	GaussSoustrait(&so,x,y);
	GaussDivise(&d,x,y);
	printf("Soustraction :");
	GaussAffiche(so,1);
	printf("Division :");
	GaussAffiche(d,1);
	int j=atoi(argv[1]);
	GaussPuiss(&pu,x,j);
	printf("Puissance %d de x:",j);
	GaussAffiche(pu,1);
	


	return 0;
}
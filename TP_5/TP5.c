#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>

/*Exercice 1
0x12ab = 11+10*2^4+2*2^8+2^12=2^0+2+2^3+(2+2^3)*2^4+2^9+2^12=2^0+2+2^3+2^5+2^7+2^9+2^12= 00000000000000000001001010101011
1+X+X^3+X^5+X^7+X^9+X^12
0xff=15+15*16 = (2^4-1)(1+2^4)=2^8-1 = 1+2+2^2+2^3+2^4+2^5+2^6+2^7= 00000000000000000000000011111111
1+X+X^2+X^3+X^4+X^5+X^6+X^7

1+X+X^7+X^8+X^10 => 0x0583
X+X^3+X^4+X^9 => 0x021a
1+X+X^2+X^3+X^16 => 0x1000f
*/
/*
void PrintBinary(long unsigned int x){
	int i=31;
	while((x&1<<i)==0){
		i--;
	}
	for(int k=i; k>=0;k--){
		if (x&1<<k) printf("1");		
		else printf("0");
	}
	printf("\n");
}
*/
void PrintBinary(long unsigned int x){
	int i = 31;
	while(((x>>i)&1)==0){
		i--;
	}
	for(int k=i;k>=0;k--){
		if((31-k)%4==0){
			printf(" ");
		}
		printf("%ld",(x>>k)&1);
	}
	printf("\n");
}

void PrintPol(long unsigned int x){
	if(x&1) printf("1");
	if( (x&(1<<1))&&(x&1) ) printf("+X");
	else if( (x&(1<<1))&&( !(x&(1<<0)) ) )printf("X");
	for(int i =2;i<32;i++){
		if (x&(1<<i)) printf("+X^%d",i);
	}
	printf("\n");
}
int Degree(long unsigned int x){
	int i=31;
	while( (x&(1<<i))==0 &&(i>=0)){
		i--;
	}
	return i;
}


int main(int argc, char *argv[]){
	//Exercice 1
	PrintBinary(121);
	PrintBinary(1<<31);
	//printf("%d",1<<32);
	//PrintBinary(1<<32);
	//Exercice 2
	PrintPol( 0x12ab );
	PrintPol( 0xff );
	PrintPol( 0x583 );
	PrintPol( 0x021a );
	PrintPol( 0x1000f );
	//Exercice 3
	printf("Degré de P: %d",Degree(0x203));
	printf("\n");

	return 0;
}
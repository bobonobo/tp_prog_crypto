#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
/*
int* TV_to_ANF(int* tab, int size){

	static int* temp;
	int l = size;
	if(l>=1){
		printf("%d\n",l);
	}
	//sleep(1);
	temp = malloc(l*sizeof(int));
	
	
	if(l>1){	
		static int* temp_sup;
		temp_sup = malloc(l/2*sizeof(int));
		static int* temp_inf;
		temp_inf= malloc(l/2*sizeof(int));
	
		for(int i =0;i<l/2;i++){
			temp_sup[i]=tab[i];
			temp_inf[i]=tab[i]^tab[i+l/2];
		}
		static int* ret_sup;
		ret_sup = malloc(l/2*sizeof(int));
		static int* ret_inf;
		ret_inf= malloc(l/2*sizeof(int));
		
		ret_sup= TV_to_ANF(temp_sup,l/2);	
		ret_inf= TV_to_ANF(temp_sup,l/2);
 		for(int i =0;i<l;i++){
			temp[i]=ret_sup[i];
			temp[i+l/2]=ret_inf[i];
		}

	}
	if(l==1){
		return tab;
	}
	return temp;
	

	return temp;
}

*/
//Exercice 1
void TV_to_ANF(int* tab, int size){
	int l = size;
	if(l>1){
		l=(l>>1);
		for (int i=0;i<l;i++){
			tab[i+l]=tab[i]^tab[i+l];
		}
		TV_to_ANF(tab,l);
		TV_to_ANF(tab+l,l);
	}
	else if (l==1){
		return;
	}

}

//Exercice 2

//0x55555555=101101101101101101101101
//0x33333333=011011011011011011011011
//0x0f0f0f0f=011111011111011111011111
//0x00ff00ff=000100010111000100010111

int poids2(unsigned int x ){
	int n=0;
	unsigned int b;
	while(x!=0){ 
		b = x;
		b=(b>>1);
		b=(b<<1);
		if ((b^x)==1){
			n++;	
		}
		x=x>>1;		
	}
	return n;
}
int poids(unsigned int x ){
	int n=0;
	while(x!=0){ 
		if ((x&1)==1){
			n++;	
		}
		x=x>>1;		
	}
	return n;
}
//*
int degre( int* tab, int size){
	int temp[size];
	int d=0;
	int j=0;
	for(int i=0;i<size;i++){
		temp[i]= tab[i];
	}
	TV_to_ANF(temp,size);
	for(int i=0;i<size;i++){
		if(temp[i]==1 && (d< (j=poids(i)) ) ) {
			d=j;
		}
	}
	return d;

}
//*/
//Exercice 3

unsigned int ProdScal1(unsigned long int x, unsigned long int y){
	unsigned long int t= x&y;
	unsigned int p=0;
	while(t!=0){
		if((t&1)==1){
			p^=1;
				
		}
		t=(t>>1);
	}
	return p;
}
//parité de 11
int parite(unsigned long int x){
	int n=0;
	while(x!=0){ 
		if ((x&1)==1){
			n^=1;	
		}
		x=x>>1;		
	}
	return n;

}
unsigned int ProdScal2(unsigned long int x, unsigned long int y){
	unsigned long int t= x&y;
	unsigned int p=parite(t);
	return p;
}

unsigned int Pi(unsigned int x){
	int pi[8]={0x0b,0x07,0x1e,0x1b,0x1d,0x0f,0xff,0x17};
	return pi[x];
}

int main(int argc, char * argv[]){
	int f[] ={0,1,1,0,0,1,0,1};
	printf("sizeof(f)=%ld \n",sizeof(f));
	int l =sizeof(f)/sizeof(int);
	int b[l];
	printf("sizeof(b)=%ld \n",sizeof(b));
	for(int i=0;i<l;i++){
		b[i]= f[i];
	}
	TV_to_ANF(b,l);
	printf("\n");
	printf("{");
	for(int i= 0;i<l-1;i++){
		printf("%d,",*(b+i));
	}
	printf("%d",*(b+l));
	printf("}");
	printf("\n");
	//*/
	int d= degre(f,l);
	printf("degre de f= %d\n",d);
	//*
	unsigned int x;
 	unsigned int y;
	for(int i=0;i<10;i++){
		for(int j=i;j<10;j++){
			x= i;
 	    	y= j;
 	    	printf("produit scalaire 1 de %d et %d = %d\n",x,y,ProdScal1(x,y));
 	    	printf("produit scalaire 2 de %d et %d = %d\n",x,y,ProdScal2(x,y));

		}
	}
 	//*
 	 //*/	
	printf("poids(0)->%d \n",poids(0));
	printf("poids(1)->%d \n",poids(1));
	printf("poids(9)->%d \n",poids(9));
	printf("poids(7)->%d \n",poids(7));
	printf("poids(5)->%d \n",poids(5));
	printf("poids(31)->%d \n",poids(31));
	printf("poids(127)->%d \n",poids(127));

	printf("poids2(0)->%d \n",poids2(0));
	printf("poids2(1)->%d \n",poids2(1));
	printf("poids2(9)->%d \n",poids2(9));
	printf("poids2(7)->%d \n",poids2(7));
	printf("poids2(127)->%d \n",poids2(127));

	printf("parite(0)->%d \n",parite(0));
	printf("parite(1)->%d \n",parite(1));
	printf("parite(7)->%d \n",parite(7));
	printf("parite(9)->%d \n",parite(9));
	printf("parite(11)->%d \n",parite(11));
	printf("parite(312)->%d \n",parite(312));
	//*
	//*/
	return 0;
}
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
/*
int* TV_to_ANF(int* tab, int size){

	static int* temp;
	int l = size;
	if(l>=1){
		printf("%d\n",l);
	}
	//sleep(1);
	temp = malloc(l*sizeof(int));
	
	
	if(l>1){	
		static int* temp_sup;
		temp_sup = malloc(l/2*sizeof(int));
		static int* temp_inf;
		temp_inf= malloc(l/2*sizeof(int));
	
		for(int i =0;i<l/2;i++){
			temp_sup[i]=tab[i];
			temp_inf[i]=tab[i]^tab[i+l/2];
		}
		static int* ret_sup;
		ret_sup = malloc(l/2*sizeof(int));
		static int* ret_inf;
		ret_inf= malloc(l/2*sizeof(int));
		
		ret_sup= TV_to_ANF(temp_sup,l/2);	
		ret_inf= TV_to_ANF(temp_sup,l/2);
 		for(int i =0;i<l;i++){
			temp[i]=ret_sup[i];
			temp[i+l/2]=ret_inf[i];
		}

	}
	if(l==1){
		return tab;
	}
	return temp;
	

	return temp;
}

*/
void TV_to_ANF(int tab[], int size){
	int l = size;
	if(l>1){
		for (int i=0;i<l/2;i++){
			tab[i+l/2]=tab[i]^tab[i+l/2];
		}
		TV_to_ANF(tab,l/2);
		TV_to_ANF(tab+l/2,l/2);
	}
	else if (l==1){
		return;
	}

}


int poids2(unsigned int x ){
	int n=0;
	unsigned int b;
	while(x!=0){ 
		b = x;
		b=(b>>1);
		b=(b<<1);
		if ((b^x)==1){
			n++;	
		}
		x=x>>1;		
	}
	return n;
}
int poids(unsigned int x ){
	int n=0;
	while(x!=0){ 
		if ((x&1)==1){
			n++;	
		}
		x=x>>1;		
	}
	return n;
}
int degre( int tab[], int size){
	int temp[size];
	int d=0;
	int j=0;
	for(int i=0;i<size;i++){
		temp[i]= tab[i];
	}
	TV_to_ANF(temp,size);
	for(int i=0;i<size;i++){
		if(temp[i]==1 && (d< (j=poids(i)) ) ) {
			d=j;
		}
	}
	return d;

}
unsigned int ProdScal1(unsigned long int x, unsigned long int y){
	unsigned long int t= x&y;
	unsigned int p=0;
	while(t!=0){
		if((t&1)==1){
			p^=1;
				
		}
		t=(t>>1);
	}
	return p;
}
//parité de 11
int parite(unsigned long int x){
	int n=0;
	while(x!=0){ 
		if ((x&1)==1){
			n^=1;	
		}
		x=x>>1;		
	}
	return n;

}
unsigned int ProdScal2(unsigned long int x, unsigned long int y){
	unsigned long int t= x&y;
	unsigned int p=parite(t);
	return p;
}

int main(int argc, char * argv[]){
	int f[] ={0,1,1,0,0,1,0,1};
	int l =sizeof(f)/sizeof(int);
	int b[l];
	for(int i=0;i<l;i++){
		b[i]= f[i];
	}
	TV_to_ANF(b,l);
	printf("\n");
	printf("{");
	for(int i= 0;i<l-1;i++){
		printf("%d,",*(b+i));
	}
	printf("%d",*(b+l));
	printf("}");
	printf("\n");
	//*/
	int d= degre(f,l);
	printf("degre de f= %d\n",d);

	printf("poids(0)->%d \n",poids(0));
	printf("poids(1)->%d \n",poids(1));
	printf("poids(9)->%d \n",poids(9));
	printf("poids(7)->%d \n",poids(7));
	printf("poids(127)->%d \n",poids(127));

	printf("poids2(0)->%d \n",poids2(0));
	printf("poids2(1)->%d \n",poids2(1));
	printf("poids2(9)->%d \n",poids2(9));
	printf("poids2(7)->%d \n",poids2(7));
	printf("poids2(127)->%d \n",poids2(127));

	printf("parite(0)->%d \n",parite(0));
	printf("parite(1)->%d \n",parite(1));
	printf("parite(7)->%d \n",parite(7));
	printf("parite(9)->%d \n",parite(9));
	printf("parite(11)->%d \n",parite(11));
	printf("parite(312)->%d \n",parite(312));
	
	return 0;
}
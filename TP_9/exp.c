#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <time.h>
int Taille(long long unsigned int x){
	if(x==0){
		return 0;
	}
	int i=31;
	while( (x&(1<<i))==0 &&(i>0)){
		i--;
	}
	return i+1;
}
long long unsigned int Mult(long long unsigned int a, long long unsigned int b){
	long long unsigned int p=0;
	for(int i=0;i<Taille(b);i++){
		if( ((b>>i)&1) ==1){
			p=p+(a<<i);	
		}		
	}
	return p;
}
long long unsigned int DivPositif(long long unsigned int *r, long long unsigned int a, long long unsigned int b){
	long long unsigned int pt;
	long long unsigned int gt;
	long long unsigned int q=0;
	if(b==1){
		*r=0;
		return a;
	}
	else if((a>b)){
		pt=b;
		gt=a;
	}
	else if((b>a)){
		*r=a;
		return 0;	
	}
	else if((b==a)){
		*r=0;
		return 1;
	}
	while(pt<=gt){
			gt=gt-pt;
			q++;
		}
	*r=gt;
	return q;		
}


long long unsigned int mulmod(long long unsigned int a, long long unsigned int n, long long unsigned int m ){
	long long unsigned int r1;
	long long unsigned int r2;
	long long unsigned int r3;
	DivPositif(&r1,a,m);
	DivPositif(&r2,n,m);
	r3=Mult(r1,r2);
	DivPositif(&r3,r3,m);
	return r3;

}

long long unsigned int sqrmod1(long long unsigned int a, long long unsigned int m){
	long long unsigned int r1;
	long long unsigned int r2;
	DivPositif(&r1,a,m);
	r2=Mult(r1,r1);
	DivPositif(&r2,r2,m);
	return r2;	
}

long long unsigned int sqrmod2(long long unsigned int a, long long unsigned int m){
	return mulmod(a,a,m);		
}
long long unsigned int expmod1(long long unsigned int a,long long unsigned int n,long long unsigned int m){
	if((n&1)==0 && n>0){
		return sqrmod1(expmod1(a,(n>>1),m),m);
	}
	else if((n&1)==1 && n>0){
		return mulmod(a,expmod1(a,n-1,m),m);
	}
	if (n==0){
		return 1;
	}
	else return -1;
}
long long unsigned int expmod2(long long unsigned int a,long long unsigned int n,long long unsigned int m){
	int t= Taille(n);
	printf("n: %lld, taille de n:%d\n",n,t);
	long long unsigned int p=1;
	long long unsigned int asq =a;//sqrmod1(a,m);
	if (n==0){
		return 1;
	}
	if((n&1)==1){	
		p=a;
	} 
	for (int i=1;i<t;i++){
		asq=sqrmod1(asq,m);
		if(((n>>i)&1)==1){
			p=mulmod(p,asq,m);
		}		
	}
	return p;
}
long long unsigned int expmod3(long long unsigned int a,long long unsigned int n,long long unsigned int m){
	int t= Taille(n);
	long long unsigned int p=1;
	long long unsigned int asq =a;//sqrmod1(a,m);
	if (n==0){
		return 1;
	}
	if((n&1)==1){	
		p=a;
	} 
	int i=1;
	while (i<t){
		asq=sqrmod1(asq,m);
		if(((n>>i)&1)==1){
			p=mulmod(p,asq,m);
		}		
		i++;
	}
	return p;
}
//long unsigned int expmod3(long unsigned int a,long unsigned int n,long unsigned int m){
//}
int main( int argc, char **argv){
	/*
	for(int i =0;i<101;i++){
		printf("expmod1 :55^%d mod 101 = %lld\n",i,expmod1(55,i,101));
		printf("expmod2 :55^%d mod 101 = %lld\n",i,expmod2(55,i,101));
		printf("expmod2 :55^%d mod 101 = %lld\n",i,expmod3(55,i,101));
	}
	//*/
	printf("expmod1 :55^100 mod 101 = %lld\n",expmod1(55,100,101));
	printf("expmod2 :55^100 mod 101 = %lld\n",expmod2(55,100,101));
	printf("expmod2 :55^100 mod 101 = %lld\n",expmod3(55,100,101));
	//RSA
	long long unsigned int m=0x5555;
	printf("message 1 0x5555:%lld\n",m);
	long long unsigned int c= expmod1(m,3,64507);
	printf("chiffré de 0x5555 = 21845 :%lld\n",c);
	//calcul trop long pour ma machine???
	long long unsigned int d= expmod1(c,42667,64507);
	printf("déchiffré de chiffré de 0x5555 =21845 :%lld\n",d);
	//b) phi(53671)=53200;
	// 53199=-1=3*(17733)===>3*(-17733)=1 ===> 3*(35467) modulo phi
	// 3 est premier avec phi(n) (3 est premier et ne divise pas phi(n))
	// On peut aussi le vérifier avec la division euclidienne
	m=12345;
	long long unsigned int e=3;
	long long unsigned int k=35467;
	long long unsigned int mod=53671;
	printf("message 2 12345:%lld\n",m);
	c= expmod1(m,e,mod);
	printf("chiffré de 12345:%lld\n",c);
	d= expmod1(c,k,mod);
	printf("déchiffré de chiffré de 12345:%lld\n",d);
	
	return 0;
}
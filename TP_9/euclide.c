#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <time.h>

unsigned int EuclideBinaire(unsigned a, unsigned b){
	if(a==b){
		return a;
	}
	if((a&1)==0 && (b&1)==0 ){
		return 2*EuclideBinaire(a>>1,b>>1);
	}
	else if((a&1)==0 && (b&1)==1 ){
		return EuclideBinaire(a>>1,b);
	}

	else if((a&1)==1 && (b&1)==0 ){
		return EuclideBinaire(a,b>>1);
	}

	else{//(a&1)==1 && (b&1)==1 
		unsigned int abs = (a-b)*(a>b)+(b-a)*(b>=a);
		unsigned int min = a*(b>a)+b*(b<=a);
		return EuclideBinaire(abs,min);
	}

}

unsigned int EuclideBinaire1(unsigned int x, unsigned int y){
	unsigned int a=x;
	unsigned int b=y;
	int d=0;
	unsigned int abs;
	unsigned int min;
	while(a!=b){
		if( (a&1)==0 && (b&1)==0 ){
			a=(a>>1);
			b=(b>>1);
			d++;
		}
		else if((a&1)==0 && (b&1)==1 ){
			a=(a>>1);
		}
		else if((a&1)==1 && (b&1)==0 ){
			b=(b>>1);
		}
		else{
			abs = (a-b)*(a>b)+(b-a)*(b>=a);
			min = a*(b>a)+b*(b<=a);
			a=abs;
			b=min;	
		}

	}
	return (1<<d)*a;

}

unsigned int EuclideBinaire2(unsigned int x, unsigned int y){
	unsigned int a=x;
	unsigned int b=y;
	int d=0;
	unsigned int abs;
	unsigned int min;
	while((a&1)==0 && (b&1)==0 ){
		a=(a>>1);
		b=(b>>1);
		d++;
	}
	while(a!=b){
		if((a&1)==0 && (b&1)==1 ){
			a=(a>>1);
		}
		else if((a&1)==1 && (b&1)==0 ){
			b=(b>>1);
		}
		else{
			abs = (a-b)*(a>b)+(b-a)*(b>=a);
			min = a*(b>a)+b*(b<=a);
			a=abs;
			b=min;	
		}

	}
	return (1<<d)*a;

}
int abs(x){
 int a = (x>=0)*x-(x<0)*x;
 return a; 	
}
int Taille(long unsigned int x){
	int i=31;
	while( (x&(1<<i))==0 &&(i>=0)){
		i--;
	}
	return i+1;
}
/*
int DivPositif(int a, int b){
	int pt;
	int gt;
	int q=0;
	if(b==1){
		return a;
	}
	else if((a>b)&&a>0&&b>0){
		pt=b;
		gt=a;
	}
	else if((b>a) && a>0 && b>0){
		return 0;	
	}
	else if((b==a) && a>0 && b>0){
		return 1;
	}
	
	int tp = Taille(pt);
	int td = Taille(gt)-tp;
	printf("td %d\n",td);
		while(pt<gt){
			gt=gt^(pt<<td);
			q+=(1<<td);
			td = Taille(gt)-tp;			
		}
	printf("reste %d\n",gt);	
	return q;		
}
//*/

int DivPositif(int a, int b){
	int pt;
	int gt;
	int q=0;
	if(b==1){
		return a;
	}
	else if((a>b)&&a>0&&b>0){
		pt=b;
		gt=a;
	}
	else if((b>a) && a>0 && b>0){
		return 0;	
	}
	else if((b==a) && a>0 && b>0){
		return 1;
	}
	
	int tp = Taille(pt);
	int td = Taille(gt)-tp;
	printf("td %d\n",td);
		while(pt<=gt){
			gt=gt-pt;
			q++;
		}
	printf("reste %d\n",gt);	
	return q;		
}


int Mult(int a, int b){
	int p=0;
	for(int i=0;i<Taille(b);i++){
		if( ((b>>i)&1) ==1){
			p=p+(a<<i);	
		}		
	}
	return p;
}

int BezoutBinaire(int*pu,int*pv,int a,int b){
	int r0;
	int r1;
	if(a<b){
		r0 = a;
		r1 = b;
	}
	else{
		r0 = b;
		r1 = a;	
	}
	int u0 = 1;
	int u1 = 0;
	int v0 = 0;
	int v1 = 1;
	int q;
	int rt0;
	int rt1;
	int ut0;
	int ut1;
	int vt0;
	int vt1;
	while((r1!=0) && (r0!=0)){
		q=DivPositif(r0,r1);
		rt0=r0;
		rt1=r1;
		r0=rt1;
		r1=rt0-Mult(q,rt1);
		ut0=u0;
		ut1=u1;
		u0=ut1;
		u1=ut0-Mult(q,ut1);
		vt0=v0;
		vt1=v1;
		v0=vt1;
		v1=vt0-Mult(q,vt1);
	}
	if(r0==0){
		*pu=ut0;
		*pv=vt0;
		return rt0; 
	}
	else{
		*pu =u0;
		*pv =v0;
		return r0;
	}
	
	

}

int main(int argc, char **argv){
	if(argc!=3){
		printf("rentrez deux valeurs à diviser, exemple: ./euclide 320 10 \n");
		return -1;
	}
	unsigned int a = (unsigned int) atoi(argv[1]);
	unsigned int b = (unsigned int) atoi(argv[2]);
	clock_t start,end;
	double t;
	start=clock();
	printf("EuclideBinaire récursif: %d\n",EuclideBinaire(a,b) );
	end = clock();
	t=((double)(end-start))/CLOCKS_PER_SEC;
	printf("durée EuclideBinaire récursif %f\n",t);	
	start=clock();
	printf("EuclideBinaire1: %d\n",EuclideBinaire1(a,b) );
	end = clock();
	t=((double)(end-start))/CLOCKS_PER_SEC;
	printf("durée EuclideBinaire1 %f\n",t);	
	start=clock();
	printf("EuclideBinaire2: %d\n",EuclideBinaire2(a,b) );
	end = clock();
	t=((double)(end-start))/CLOCKS_PER_SEC;
	printf("durée EuclideBinaire2 %f\n",t);
	/*
	for(int i=1;i<11;i++){
		for(int j=1;j<11;j++){
		printf("produit de %d et %d : %d\n",i,j,Mult(i,j));
		}	
	}
	*/

	for(int i=1;i<11;i++){
		for(int j=1;j<11;j++){
			printf("division de %d par %d : %d\n",i,j,DivPositif(i,j));
		}	
	}
	//*
	int u;
	int v;
	int r = BezoutBinaire(&u,&v,15,17);
    printf("(%d)*(%d) +(%d)*(%d) = %d \n",u,15,v,17,r);
    r = BezoutBinaire(&u,&v,16,32);
    printf("(%d)*(%d) +(%d)*(%d) = %d \n",u,16,v,32,r);
    r = BezoutBinaire(&u,&v,14,21);
    printf("(%d)*(%d) +(%d)*(%d) = %d \n",u,14,v,21,r);
    r = BezoutBinaire(&u,&v,31,14);
    printf("(%d)*(%d) +(%d)*(%d) = %d \n",u,14,v,31,r);
    
    //*/	
	return 0;	
	
}
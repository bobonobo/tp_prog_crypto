#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>


long unsigned int PolMul(long unsigned int a, long unsigned int b){
	int p =0;
	for(int i=0;i<32;i++){
		p^=(a<<i)*((b>>i)&1);
	}
	return p;
}
void AfficheN(long unsigned int x){
	int i = 31;
	if(x==0){
		printf("%ld",x);
		return;	
	}
	while(((x>>i)&1)==0){
		i--;
	}
	for(int k=i;k>=0;k--){
		if((31-k)%4==0){
			printf(" ");
		}
		printf("%ld",(x>>k)&1);
	}
	printf("\n");
}
int Degree(long unsigned int x){
	int i=31;
	while( (x&(1<<i))==0 &&(i>=0)){
		i--;
	}
	return i;
}

long unsigned int PolSqr(long unsigned int a){
	int p =0;
	for(int i=0;i<32;i++){
		p^=(a<<i)*((a>>i)&1);
	}
	return p;
}

long unsigned int PolModDiv(long unsigned int *r, long unsigned int a, long unsigned int b){
	long unsigned int q=0;	
	int db =Degree(b);
	int d = Degree(a)-db;
	r[0]=a;
	int i=0;
	printf("r%d:",i);
	AfficheN(r[0]);
	if(a<=b){
		return q;
	}
	else{
		while(r[0]>b){
			r[0]= r[0]^(b<<d);
			i++;
			printf("r%d:",i);
			AfficheN(r[0]);
			q+=(1<<d);
			printf("q%d:",i);
			AfficheN(q);
			d=Degree(r[0])-db;	
		}
		return q;
	}

}
int pzero(long unsigned int x){
	int i=0;
	while(((x>>i)&1)==0){
		i++;
	}
	return i;
}
/*
long unsigned int PowerSerial(long unsigned int a, long unsigned int b){
	long unsigned int r=a;
	long unsigned int q=0;
	int d = Degree(a);
	int div=b;
	int i=0;
	int p;
	printf("r%d:",i);
	AfficheN(r);
	if(a<=b){
		return q;
	}
	else if ((b & 1)!=0){
		while (Degree(div)<=d){
			p=pzero(r);
			div=(b<<p);
			r= r^div;
			q+=(1<<p);
			i++;
			printf("r%d:",i);
			AfficheN(r);
			printf("q%d:",i);
			AfficheN(q);
		}
	return q;
	}
	else return q;


}
*/

long unsigned int PowerSerial(long unsigned int a, long unsigned int b){
	long unsigned int r=a;
	long unsigned int q=0;
	int d = Degree(a);
	int div=b;
	int i=0;
	int p;
	printf("r%d:",i);
	AfficheN(r);
	if(a<=b){
		return q;
	}
	else if ((b & 1)!=0){
		while (Degree(div)<=31){
			p=pzero(r);
			div=(b<<p);
			r= r^div;
			q+=(1<<p);
			i++;
			printf("r%d:",i);
			AfficheN(r);
			printf("q%d:",i);
			AfficheN(q);
		}
	return q;
	}
	else return q;


}

int main(int argc, char *argv[]){
	long unsigned int p= PolMul(0xa2,0x225);
	printf("multiplication du polynôme " ); 	
	AfficheN(0xa2);
	printf("et " );
	AfficheN(0x225);
	printf(" : " );
	AfficheN(p);

	long unsigned int sq = PolSqr(0x1a);
	printf("polynôme au carré ");
	AfficheN(0x1a);
	printf(" : " );
	AfficheN(sq);

	printf("division euclidienne de ");
	AfficheN(0x33a);
	printf("par " );
	AfficheN(0xb);
	long unsigned int r=0;
	long unsigned int *ptr =&r;
	long unsigned int q = PolModDiv(ptr,0x33a,0xb);
	printf("Q: ");
	AfficheN(q); 
	printf("R: ");
	AfficheN(r);
	long unsigned int q2 = PowerSerial(0x1252,0x19);
	printf("Q puissances croissantes: ");
	AfficheN(q2); 



	return 0;
}
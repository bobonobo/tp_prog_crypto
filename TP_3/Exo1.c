#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>

int puiss(int n){
	int p=1;
	int c=0;
	while(p<n){
		p<<=1;
		c++;
	}
	if(p==n){
		return c;
	}
	else{
		return -1;
	}
}

int poids(unsigned int x ){
	int n=0;
	while(x!=0){ 
		if ((x&1)==1){
			n++;	
		}
		x=x>>1;		
	}
	return n;
}

void Fourier(int* tab, int size){
	int l = size;
	int p = puiss(size);
	if(p!=-1){
		if(l>1){
			l=(l>>1);
			int temp=0;
			for (int i=0;i<l;i++){
				temp=tab[i];
				tab[i]+=tab[i+l];
				tab[i+l]=temp-tab[i+l];
			}
			Fourier(tab,l);
			Fourier(tab+l,l);
		}
		else if (l==1){
			return;
		}
	}

}
void Fourier_It(int* tab, int n){
	unsigned int nn=1<<n;
	unsigned int rr=nn>>1;
	int i;
	int t;
	do{
		i=0;
		do{
			t=tab[i];
			tab[i]=tab[i+rr]+tab[i];
			tab[i+rr]=t-tab[i+rr];
			i++;
			if((i&(rr-1))==0){
				i+=rr;
			}
		}
		while(i<nn);
		rr>>=1;
	}
	while(rr!=0);
}

void Walsh(int* tab, int size){
	for (int i=0;i<size;i++){
		if(tab[i]==0){
			tab[i]=1;	
		}
		else{
			tab[i]=-1;	
		}
	}
	Fourier(tab,size);
}

void Walsh_somme(int* tab, int size){
	Fourier(tab,size);
	int p = puiss(size);
	if(p!=-1){
		for(int i=0;i<size;i++){
			if(i==0){
				tab[i]=size-2*tab[i];
			}
			else{
				tab[i]=-2*tab[i];	
			}
		}
	}
}
int resilience(int *f, int n){
	int t=1<<n;
	Walsh(f,t);
	int or=0;
	for(int i =0;i<t;i++){
		while( f[i]==0 ) {
			if(or<poids(i)){
				or=poids(i);	
			}			
		}
	}
	return or;
}
unsigned int Pi(unsigned int x){
	int pi[8]={0x0b,0x07,0x1e,0x1b,0x1d,0x0f,0xff,0x17};
	return pi[x];
}




int main(int argc, char * argv[]){
	int f[]={0,1,1,0,0,1,0,1};
	int l = sizeof(f)/sizeof(int);
	int t[l];
	int ti[l];
	int x[l];	
	int w[l];
	int p= puiss(l);
	for(int i =0;i<l;i++){
		t[i]=f[i];
		ti[i]=f[i];
		x[i]=f[i];
		w[i]=f[i];
	}
	Fourier(t,l);
	Fourier_It(ti,p);
	Walsh(x,l);
	Walsh_somme(w,l);

	printf(" TF : \n");
	for(int i =0;i<l;i++){
		printf("%d ",t[i]);
	}
	printf("\n");

	printf(" TF itératif: \n");
	for(int i =0;i<l;i++){
		printf("%d ",ti[i]);
	}
	printf("\n");
	printf(" X : \n");
	for(int i =0;i<l;i++){
		printf("%d ",x[i]);
	}
	printf("\n");
	printf(" W : \n");
	for(int i =0;i<l;i++){
		printf("%d ",w[i]);
	}
	printf("\n");

	return 0;
}


